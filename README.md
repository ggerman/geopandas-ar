# geopandas-ar

- Install Docker in to Debian [howto](https://docs.docker.com/install/linux/docker-ce/debian/)

Use Docker like user non-root (this way is more easy to use)
```
Like root
# usermod -aG docker my-user-name
Or using sudo
# sudo usermod -aG docker $(whoami)

```


INSTRUCTIONS to use the container with geopandas

```
# git clone https://gitlab.com/ggerman/geopandas-ar
# cd geopandas-ar
# docker build -t geopandas . < Dockerfile
# docker run -it --rm --name geo -v ./app/:/usr/src/myapp -w /usr/src/myapp geopandas python main.py
```

This project have like target build friendly way to buil gis images of geo information by region.

- [x] Dockerized geopandas
- [x] Write tutorial for run applcations using dockers containers
- [ ] Build a easy way to get the Regions and plot the backend of maps
- [ ] Build a website with a steps to choice region (Country, States, Cities) 
- [ ] Research by grids like [this widget](https://dhtmlx.com/docs/products/dhtmlxSpreadsheet/)
- [ ] Allow upload xls, xlsx, odc, csv files
- [ ] Export images jpg, png, gif, ect ...


